# OSC_BetterPIHeaders

This contribution alters the product info header to display the manufacturers name as a link to a listing of their products below the product's name. This replaces the product ID.

For more information see http://addons.oscommerce.com/info/6982

## Instalation
1. Download the latest version (https://bitbucket.org/matthewlinton/osc_betterpiheaders/downloads)
1. Unzip the package
1. Switch to the "OSC_BetterPIHeaders" directory
1. Follow the directions in "better_product_info_headers.txt